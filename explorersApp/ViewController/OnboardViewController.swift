//
//  OnboardViewController.swift
//  explorersApp
//
//  Created by Priscilla Vanny Amelia on 03/05/21.
//

import UIKit

struct Explorers: Codable {
    var Name: String
    var Photo: String
    var Expertise: String
    var Team: String
    var Shift: String
}
var dataParsed: Array<Explorers> = []

//class OnboardViewController: UIViewController, UITableViewDelegate, UITableViewDataSource { // dipakai kalau pake tableview
class OnboardViewController: UIViewController {
    
    @IBOutlet var holderView: UIView!
    let scrollView = UIScrollView()
    var pageView = UIView()
    public var lblName = UILabel()
    var tvInterest = UITextView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))

        view.addGestureRecognizer(tap)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        /*
        // read data from api
        let urlString = "https://nc2.theideacompass.com/explorers-api.json"
        self.loadJson(fromURLString: urlString){ (result) in
            switch result{
            case .success(let data):
                dataParsed = self.parse(jsonData: data)
            case .failure(let error):
                print("load json url error: ", error)
            }
        }
        */
        
        // read the local JSON data
        if let localData = self.readLocalFile(forName: "explorers"){
            dataParsed = self.parse(jsonData: localData)
//            print(dataParsed)
        }
        
        configure()
    }
    
    private func readLocalFile(forName name: String) -> Data? {
        do {
            if let bundlePath = Bundle.main.path(forResource: name, ofType: "json"),
               let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8){
                return jsonData
            }
        } catch {
            print("read local file error: ", error)
        }
        return nil
    }
    
    private func loadJson(fromURLString urlString: String, completion: @escaping (Result<Data, Error>) -> Void){
        if let url = URL(string: urlString){
            let urlSession = URLSession(configuration: .default).dataTask(with: url){ (data, response, error) in
                if let error = error{
                    completion(.failure(error))
                }
                
                if let data = data {
                    completion(.success(data))
                }
            }
            urlSession.resume()
        }
    }
    
    private func parse(jsonData: Data) -> Array<Explorers> {
        var explorArray: Array<Explorers> = []
        do{
            let decodedData = try JSONDecoder().decode([Explorers].self, from: jsonData) // this will be returned
            explorArray = decodedData
            return decodedData
        }catch{
            print("parsing error:", error)
        }
        return explorArray
    }
    
    private func configure(){
//        setup scrollview
        scrollView.frame = holderView.bounds
        holderView.addSubview(scrollView)
        let titles = ["Get Started", "Confirmation"]
        for x in 0..<2{
            pageView = UIView(frame: CGRect(x: CGFloat(x) * holderView.frame.size.width, y:0, width: holderView.frame.size.width, height: holderView.frame.size.height))
            scrollView.addSubview(pageView)
            
//            Button
            let btConfirm = UIButton(frame: CGRect(x: 55, y: 450, width: 280, height: 45))
//            btConfirm.frame = CGRect(x: 55, y: 450, width: 280, height: 45)
            btConfirm.setTitleColor(.white, for: .normal)
            btConfirm.backgroundColor = #colorLiteral(red: 1, green: 0.6476001143, blue: 0.05892718583, alpha: 1) //pakai ColorLiteral
//            btConfirm.isHidden = true
            btConfirm.setTitle("Done", for: .normal)
            btConfirm.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .regular)
            btConfirm.addTarget(self, action: #selector(didTapButton(_:)), for: .touchUpInside)
            btConfirm.tag = x+1
            btConfirm.layer.cornerRadius = 5
            pageView.addSubview(btConfirm)
            
//            Title Label
            let lblTitle = UILabel(frame: CGRect(x: 35, y: 30, width: pageView.frame.size.width-20, height: 80))
            lblTitle.textAlignment = .left
            lblTitle.font = UIFont.systemFont(ofSize: 33, weight: .bold)
            pageView.addSubview(lblTitle)
            lblTitle.text = titles[x]
            
            if x==0 {
                // Name View
                let nameView = UIView(frame: CGRect(x: 0, y: 140, width: pageView.frame.size.width, height: 45))
                nameView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.2470588235)
                pageView.addSubview(nameView)
        
                let lblNamet = UILabel(frame: CGRect(x: 25, y: 0, width: 50, height: nameView.frame.size.height))
                lblNamet.text = "Name"
                lblNamet.font = UIFont.systemFont(ofSize: 14, weight: .regular)
                nameView.addSubview(lblNamet)
                
                lblName.frame = CGRect(x: nameView.frame.size.width-210, y: 0, width: 200, height: nameView.frame.size.height)
                lblName.text = "Choose One"
                lblName.textAlignment = .right
                lblName.textColor = #colorLiteral(red: 0.03529411765, green: 0.5176470588, blue: 1, alpha: 1)
                lblName.font = UIFont.systemFont(ofSize: 14, weight: .regular)
                nameView.addSubview(lblName)
                
                let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(gestureFired(_:)))
                gestureRecognizer.numberOfTapsRequired = 1
                gestureRecognizer.numberOfTouchesRequired = 1
                
                nameView.addGestureRecognizer(gestureRecognizer)
                nameView.isUserInteractionEnabled = true
                
                // textview
                let interView = UIView(frame: CGRect(x: 0, y: 210, width: pageView.frame.size.width, height: 160))
                interView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.2470588235)
                pageView.addSubview(interView)
                
                let lblInterest = UILabel(frame: CGRect(x: 25, y: 2, width: interView.frame.size.width, height: 20))
                lblInterest.text = "Interest"
                lblInterest.font = UIFont.systemFont(ofSize: 14, weight: .regular)
                interView.addSubview(lblInterest)
                
//                let tvInterest = UITextView(frame: CGRect(x: 5, y: 25, width: interView.frame.size.width-10, height: interView.frame.size.height-30))
                tvInterest.frame = CGRect(x: 5, y: 25, width: interView.frame.size.width-10, height: interView.frame.size.height-30)
                tvInterest.backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 0)
                interView.addSubview(tvInterest)
            }else{
                btConfirm.setTitle("Yes, it's me!", for: .normal)
                let btNotMe = UIButton(frame: CGRect(x: 145, y: 525, width: 110, height: 20))
                btNotMe.setTitleColor(.black, for: .normal)
                btNotMe.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.8470588235), for: .normal) //pakai ColorLiteral
                btNotMe.isEnabled = true
                btNotMe.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .regular)
                btNotMe.setTitle("No, it's not me", for: .normal)
                btNotMe.addTarget(self, action: #selector(didTapButton(_:)), for: .touchUpInside)
                btNotMe.tag = x-1
                pageView.addSubview(btNotMe)
            }
            view.endEditing(true)
        }
        
        scrollView.contentSize = CGSize(width: holderView.frame.size.width*2, height: 0)
        scrollView.isPagingEnabled = true
        scrollView.isScrollEnabled = false
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @objc func gestureFired(_ gesture: UITapGestureRecognizer){
        let vc = storyboard?.instantiateViewController(identifier: "nametable") as! NameViewController
        vc.modalPresentationStyle = .fullScreen
        vc.onboard = self
        present(vc, animated: true)
    }
    
    @objc func didTapButton(_ button: UIButton){
        guard button.tag < 2 else{
            // dismiss
            Core.shared.notNewUser()
            Core.shared.defaults.setValue(tvInterest.text, forKey: "interest")
            dismiss(animated: true, completion: nil) //completion: completion handler
            return
        }
        //scroll to the next page
        if lblName.text != "Choose One"{
            if let foto = Core.shared.defaults.value(forKey: "userPhoto") as? String{
                let iUser = UIImage(named: foto)
                let ivUser = UIImageView(image: iUser!)
                ivUser.frame = CGRect(x: 95, y: 230, width: (iUser?.size.width)!/5, height: (iUser?.size.height)!/5)
                ivUser.layer.cornerRadius = 50
                ivUser.clipsToBounds = true
                pageView.addSubview(ivUser)
            }
            scrollView.setContentOffset(CGPoint(x: holderView.frame.size.width * CGFloat(button.tag), y: 0), animated: true)
        }
    }
    
    public func updateName(){
        if let name = Core.shared.defaults.value(forKey: "namaUser") as? String{
            lblName.text = name
        }
    }
}

class Core{
    static let shared = Core()
    let defaults = UserDefaults(suiteName: "com.test.saved.data")!
    
    func isNewUser() -> Bool{
        return !UserDefaults.standard.bool(forKey: "isNewUser") // UserDefaults.standar.bool returns false kalau keynya belum ada
    }
    
    func notNewUser(){
        UserDefaults.standard.set(true, forKey: "isNewUser")
    }
}


/* LOG
 4 Mei: coba buat onboard dengan tableview tapi belum selesai (elemen tombol dan label judul jadi ga muncul krn tableview, tableview isi sama semua)
 5 Mei: ukuran tableview sudah aman, tidak menutupi (set tableview.frame jangan bounds)
 */
