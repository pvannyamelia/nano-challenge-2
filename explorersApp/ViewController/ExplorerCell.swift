//
//  ExplorerCell.swift
//  explorersApp
//
//  Created by Priscilla Vanny Amelia on 06/05/21.
//

import UIKit

class ExplorerCell: UITableViewCell {
    
    @IBOutlet weak var nama: UILabel!
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var expertise: UILabel!
    @IBOutlet weak var shift: UILabel!
    
    func UpdateCellView(explorer: ExplorerModel){
        photo.image = UIImage(named: explorer.Photo)
        nama.text = explorer.Name
        expertise.text = explorer.Expertise
        shift.text = explorer.Shift
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
