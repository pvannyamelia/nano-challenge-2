//
//  ViewController.swift
//  explorersApp
//
//  Created by Priscilla Vanny Amelia on 30/04/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var btnLogin: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnLogin.isEnabled = false
        btnLogin.isHidden = true
        // Do any additional setup after loading the view.
    }

    @IBAction func btLogin(_ sender: Any) {
        if Core.shared.isNewUser(){
            let vc = storyboard?.instantiateViewController(identifier: "onboard") as! OnboardViewController
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true)
        }else{
            let vc = storyboard?.instantiateViewController(identifier: "tabbarmain") as! TabBarViewController
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true)
        }
    }
}

