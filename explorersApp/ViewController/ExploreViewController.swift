//
//  MainViewController.swift
//  explorersApp
//
//  Created by Priscilla Vanny Amelia on 05/05/21.
//

import UIKit

class ExploreViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var explorerTableView: UITableView!
    var explorers: Array<ExplorerModel> = []
    @IBOutlet weak var searchbar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        explorerTableView.delegate = self
        explorerTableView.dataSource = self
        
//        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
//        tap.cancelsTouchesInView = false
//        view.addGestureRecognizer(tap)
    }
    
    override func viewDidLayoutSubviews() {
        if let localData = self.readLocalFile(forName: "explorers"){
            dataParsed = self.parse(jsonData: localData)
            create_explorer()
        }
    }
    
    private func readLocalFile(forName name: String) -> Data? {
        do {
            if let bundlePath = Bundle.main.path(forResource: name, ofType: "json"),
               let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8){
                return jsonData
            }
        } catch {
            print("read local file error: ", error)
        }
        return nil
    }
    
    private func parse(jsonData: Data) -> Array<Explorers> {
        var explorArray: Array<Explorers> = []
        do{
            let decodedData = try JSONDecoder().decode([Explorers].self, from: jsonData) // this will be returned
            explorArray = decodedData
            return decodedData
        }catch{
            print("parsing error:", error)
        }
        return explorArray
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func create_explorer(){
        for i in 0...dataParsed.count-1 {
            explorers.append(ExplorerModel(photo: dataParsed[i].Photo, nama: dataParsed[i].Name, expertise: dataParsed[i].Expertise, shift: dataParsed[i].Shift))
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(explorers.count)
        return explorers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "explorerCell", for: indexPath) as? ExplorerCell {
            let extractedExplorer = explorers[indexPath.row]
            cell.UpdateCellView(explorer: extractedExplorer)
            return cell
        }else{
            return ExplorerCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = storyboard?.instantiateViewController(identifier: "otherExp") as! OtherExplorerViewController
        vc.modalPresentationStyle = .fullScreen
        vc.explore = self
        print(type(of: dataParsed[indexPath.row].Photo))
        print("tap")
        present(vc, animated: true)
        vc.dpExp.image = UIImage(named: dataParsed[indexPath.row].Photo)
        vc.lbNama.text = dataParsed[indexPath.row].Name
        vc.lbExpertise.text = dataParsed[indexPath.row].Expertise + " ・ " + dataParsed[indexPath.row].Shift
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135.0;//Choose your custom row height
    }
    
    /*
    // MARK: - LOG

    7 Mei: tableview items ga muncul tapi selnya muncul, ternyata karena parsing data cuma 1 kali di onboard (onboard hanya dilakukan pas app pertama kali launch), di halaman ini belum ada
    */

}
