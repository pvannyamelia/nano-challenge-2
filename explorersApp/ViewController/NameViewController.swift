//
//  NameViewController.swift
//  explorersApp
//
//  Created by Priscilla Vanny Amelia on 05/05/21.
//

import UIKit

class NameViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var nameHolder: UIView!
    var onboard = OnboardViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableConfig()
    }
    
    private func tableConfig(){
        let svName = UIScrollView()
        svName.frame = nameHolder.bounds
        nameHolder.addSubview(svName)
        
        let tableView: UITableView = {
            let table = UITableView(frame: .zero, style: .grouped)
            table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
            return table
        }()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.frame = svName.bounds
        svName.addSubview(tableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataParsed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = dataParsed[indexPath.row].Name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: true)
        Core.shared.defaults.setValue(dataParsed[indexPath.row].Name, forKey: "namaUser")
        Core.shared.defaults.setValue(dataParsed[indexPath.row].Photo, forKey: "userPhoto")
        Core.shared.defaults.setValue(dataParsed[indexPath.row].Expertise, forKey: "userExp")
        Core.shared.defaults.setValue(dataParsed[indexPath.row].Shift, forKey: "userShift")
        Core.shared.defaults.setValue(dataParsed[indexPath.row].Team, forKey: "userTeam")
        onboard.updateName()
        self.dismiss(animated: true, completion: nil)
    }
}


/* Log
 5 Mei: coba parse untuk dimasukkan data nama, tapi gagal
 
 parsing error: typeMismatch(Swift.Dictionary<Swift.String, Any>, Swift.DecodingError.Context(codingPath: [], debugDescription: "Expected to decode Dictionary<String, Any> but found an array instead.", underlyingError: nil))
 parsing error: typeMismatch(Swift.Dictionary<Swift.String, Any>, Swift.DecodingError.Context(codingPath: [], debugDescription: "Expected to decode Dictionary<String, Any> but found an array instead.", underlyingError: nil))


 */
