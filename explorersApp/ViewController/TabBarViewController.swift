//
//  TabBarViewController.swift
//  explorersApp
//
//  Created by Priscilla Vanny Amelia on 05/05/21.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        if Core.shared.isNewUser(){
            let vc = storyboard?.instantiateViewController(identifier: "onboard") as! OnboardViewController
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true)
        }
    }

}
