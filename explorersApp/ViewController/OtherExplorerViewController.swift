//
//  OtherExplorerViewController.swift
//  explorersApp
//
//  Created by Priscilla Vanny Amelia on 07/05/21.
//

import UIKit

class OtherExplorerViewController: UIViewController {
    
    @IBOutlet var dpExp: UIImageView!
    @IBOutlet var lbNama: UILabel!
    @IBOutlet var lbExpertise: UILabel!
    
    @IBAction func backbt(_ sender: UIButton!){
        self.dismiss(animated: true, completion: nil)
    }
    public var explore = ExploreViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }
    
    /*
    // MARK: - Navigation

     // In a storyboard-based apication, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
