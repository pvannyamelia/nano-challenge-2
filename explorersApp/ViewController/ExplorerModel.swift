//
//  ExplorerModel.swift
//  explorersApp
//
//  Created by Priscilla Vanny Amelia on 06/05/21.
//

import Foundation

struct ExplorerModel{
    var Photo: String
    var Name: String
    var Expertise: String
    var Shift: String
    
    init(photo: String, nama: String, expertise: String, shift: String){
        self.Photo = photo
        self.Name = nama
        self.Expertise = expertise
        self.Shift = shift
    }
}
