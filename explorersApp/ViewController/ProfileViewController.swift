//
//  ProfileViewController.swift
//  explorersApp
//
//  Created by Priscilla Vanny Amelia on 07/05/21.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet var ivDisPic: UIImageView!
    @IBOutlet var lbNama: UILabel!
    @IBOutlet var lbExpert: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        let name = Core.shared.defaults.value(forKey: "namaUser") as? String
        let expertise = Core.shared.defaults.value(forKey: "userExp") as? String
        let shift = Core.shared.defaults.value(forKey: "userShift") as? String
        let photo = Core.shared.defaults.value(forKey: "userPhoto") as? String
        
        if name !=  nil {
            lbNama.text = name
            lbExpert.text =  expertise! + " ・ " + shift!
            ivDisPic.image = UIImage(named: photo!)
        }
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
